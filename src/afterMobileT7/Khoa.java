package afterMobileT7;

public class Khoa {

    String nameKh;
    String nameSchool ;
    String nameClass;
    int slKhoa ;
    float diemTungKhoa ;
    double diemToanBoKhoa ;



    // 1 . Viet cac hàm kiểu dữ liệu  : int ,float ,string ,double
    // Tái sử dụng các hàm kiểu dữ liệu : int ,float ,string ,double


    //1 . Hàm trả về kiểu dữ liệu float

    public float getDiemTungKhoa(String nameKhoa ) {

        if (nameKhoa.contentEquals("CNTT")) {
            diemTungKhoa = 900;

        }
        else
        {
            System.out.print("Not Found Ten Khoa :  " + nameKhoa);
        }
        return  diemTungKhoa ;

    }


    //1 . Hàm trả về kiểu dữ liệu int

    public int getSlKhoa (String nameSchool) {
        if(nameSchool.equals("DHCNTT")) {
            slKhoa = 10;
        }
        else
        {
            System.out.print("Not Found Name School: " +nameSchool );
        }
        return  slKhoa;
    }


    //3 . Ham tra ve double
    public double getDiemToanBoKhoa(String nameSchool ) {

        if (nameSchool.contentEquals("DHCNTT")) {
            diemToanBoKhoa = 900;

        }
        else
        {
            System.out.print("Not Found Diem Toan Bo Khoa of  :  " + nameSchool);
        }
        return  diemToanBoKhoa ;

    }


    // Tai su dung cac ham phia tren : Int  ,Float ,String ,Double
    public double getTotalFollowNameHs(String nameHs ) {
        double total ;
        diemToanBoKhoa = getDiemToanBoKhoa("DHCNTT");
        diemTungKhoa = getDiemTungKhoa("CNTT");
        total = diemToanBoKhoa+diemTungKhoa;
        return  total;
    }






    // 2. 1 class có ít nhất 3 constructor
    Khoa() {
        // default
    }

    Khoa(String nameKh) {
        this.nameKh = nameKh;
    }

    Khoa(String nameKh,String nameClass) {
        this.nameClass = nameClass ;
        this.nameKh = nameKh ;
    }  //using constructor

    Khoa(String nameSchool,String nameKh,float diemToanBoKhoa) {
        this.nameSchool = nameSchool ;
        this.nameKh = nameKh ;
        this.diemToanBoKhoa = diemToanBoKhoa ;
    }







}
