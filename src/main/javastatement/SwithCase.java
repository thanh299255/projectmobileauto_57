package main.javastatement;

public class SwithCase {

    public static void getInt() {

        int num = 2;  // variable
        //  num+2 : an integer expression
        // 12  : an integer expression
        switch (4) {
            case 1:
                System.out.println("Case1: Value is: " + num);
                break;
            case 2:
                System.out.println("Case2: Value is: " + num);
                break;
            case 3:
                System.out.println("Case3: Value is: " + num);
                break;
            default:
                System.out.println("Default: Value is: " + num);

                //Noted :  Chưa có kết hợp Break ;
//1.default sẽ là else nếu giá trị variable or an integer expression không rơi vào trường hợp case nào hết
//2. Khi giá trị (variable or an integer expression) rơi vào 1 case bất kỳ trong Swith case ==> nó lấy tất cả giá trị
// trị tính từ case đó đến hết hết default  .


//Noted :  Có kết hợp Break ;
//1 . Khi có Break thì nó chỉ lấy đúng giá trị key ( variable or an integer expression) của case và kết thúc ngay



// Key của key (variable or an integer expression) không được giống nhau trong các key

        }

    }

        public static void getTest(String month) {
          // variable
            //  num+2 : an integer expression
            // 12  : an integer expression
            switch (month) {
                case "1":
                    System.out.println("Case1: Value is: " + 30);
                    break;
                case "2":
                    System.out.println("Case2: Value is: " + 28);
                    break;
                case "3":
                    System.out.println("Case3: Value is: " + 31);
                    break;
                default:
                    System.out.println("Default: Value is: " + 31);

            }
        }
}