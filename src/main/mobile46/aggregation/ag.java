package main.mobile46.aggregation;

class Address1
{
    int streetNum;
    String city;
    String state;
    String country;
    Address1(int street, String c, String st, String coun)
    {
        this.streetNum=street;
        this.city =c;
        this.state = st;
        this.country = coun;
    }
}
class StudentClass1
{
    int rollNum;
    String studentName;
    //Creating HAS-A relationship with Address class
    Address1 studentAddr;


    StudentClass1(int roll, String name, Address1 addr){
        this.rollNum=roll;
        this.studentName=name;
        this.studentAddr = addr;
    }
}
class College
{
    String collegeName;
    //Creating HAS-A relationship with Address class
    Address collegeAddr;
    College(String name, Address addr){
        this.collegeName = name;
        this.collegeAddr = addr;
    }

}
class Staff
{
    String employeeName;
    //Creating HAS-A relationship with Address class
    Address employeeAddr;
    Staff(String name, Address addr){
        this.employeeName = name;
        this.employeeAddr = addr;
    }

}
