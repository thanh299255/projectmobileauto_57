package main.mobile46.supperkey;

//Parent class or Superclass or base class
class Superclass
{
    int num = 100;  // supper key

}



//Child class or subclass or derived class
class Subclass extends Superclass
{
    /* The same variable num is declared in the Subclass
     * which is already present in the Superclass
     */
    int num = 110;

    void printNumber(){
        System.out.println(num);

    }  //method


    public static void main(String args[]){
        Subclass obj= new Subclass();
        obj.printNumber();
    }
}

