package main.mobile46.supperkey;

class Parentclass
{
    //no-arg constructor
    Parentclass(){
        System.out.println("no-arg constructor of parent class");
    }
    //arg or parameterized constructor
    Parentclass(String str,int b){
        System.out.println("parameterized constructor of parent class");
    }
    Parentclass(String str,int b,String c){
        System.out.println("parameterized constructor of parent class");
    }


}
class Subclass1 extends Parentclass
{
    Subclass1(){
        /* super() must be added to the first statement of constructor
         * otherwise you will get a compilation error. Another important
         * point to note is that when we explicitly use super in constructor
         * the compiler doesn't invoke the parent constructor automatically.
         */
        super("Hahaha",10);  //super keyword
        System.out.println("Constructor of child class");

    }


    Subclass1(String c){
        super("Hahaha",10,"111");  //super keyword
        System.out.println("Constructor of child class");

    }

    Subclass1(int c){
        super("Hahaha",10,"111");  //super keyword
        System.out.println("Constructor of child class");

    }

//    Parentclass a =new Parentclass("111",10,"111") ;

    void display(){
        System.out.println("Hello");
    }
    public static void main(String args[]){
        Subclass1 obj= new Subclass1();
        obj.display();
    }
}

