package main.mobile46.abstactclass.contructor;

public class ConstructorSC {
    String  firstName;
    int age ;
    String  lastName;

    ConstructorSC() {
        this.age =222;  //state xu ly
   }  //1

//    ConstructorSC() {
//    }

//     ==> // single constructor

    ConstructorSC(String firstName){
        this(firstName,8);
    }//2
    ConstructorSC(String firstName,int age){
        this(firstName,age,"Phan");
    }//3


    ConstructorSC(String firstName,int age,String lastName){
        this.firstName = firstName;
        this.age = age ;
        this.lastName= lastName;

    }//4



//    ConstructorSC ob1 =new ConstructorSC(getFirstName(),getAge(),getlastName());



    // 3 sẽ lấy dữ liệu 2,1
    // 2 sẽ lấy dữ liệu của 1








}
