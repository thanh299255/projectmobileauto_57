package main.mobile46.abstactclass.contructor;

public class ClassParent {

    public String getFirstName() {
        String firstName ="Thanh";
        // statement code
        return firstName ;
    }
    public int getAge() {
        int age  =20;
        // statement code
        return age ;
    }

    public String getlastName() {
        String lastName ="Phan";
        // statement code
        return lastName ;
    }
}
