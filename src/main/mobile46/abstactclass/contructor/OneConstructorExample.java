package main.mobile46.abstactclass.contructor;

public class OneConstructorExample {

    int age;
    String name;

    //Default constructor
    OneConstructorExample (){
        this.name="Chaitanya";
        this.age=30;
    }  // dạng gắn giá trị trực tiếp

    //Parameterized constructor
    OneConstructorExample (String n,int a){
        this.name=n;
        this.age=a;
    }  //dạng xử dụng params


    //unit test
    public static void main(String args[]){
        OneConstructorExample obj1 = new OneConstructorExample ();
        OneConstructorExample obj2 =
                new OneConstructorExample ("Steve", 56);
        System.out.println(obj1.name+" "+obj1.age);
        System.out.println(obj2.name+" "+obj2.age);
    }
}

