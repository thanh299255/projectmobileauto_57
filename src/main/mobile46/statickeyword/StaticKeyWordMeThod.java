package main.mobile46.statickeyword;

class StaticKeyWordMeThod
{
    // This is a static method

    // keyword method static
    static void myMethod()   // public static or static
    {
        int i =10 ;
        if (i>0 ) {
            System.out.println("Automation Static Method");
        } else
        {
            System.out.println("-----");
        }
    }


    // vid 2
   // int ,float ,double
    static String myMethod1()   // public static or static
    {
        int i =10 ;
        if (i>0 ) {
            System.out.println("Automation Static Method");
        } else
        {
            System.out.println("-----");
        }
        return  null;
    }





    public static void main(String[] args)
    {
        /* You can see that we are calling this
         * method without creating any object.
         */
        myMethod();
    }
}
