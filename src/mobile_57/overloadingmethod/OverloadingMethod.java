package mobile_57.overloadingmethod;

public class OverloadingMethod {


    public int getSL() {
        return  0;
    } //1

    public int getSL(int n) {
        return  n;
    } //2

    public float getSL( float n) {
        return  n;
    } //3


    public float getSL( float n,int a) {
        return  n + a ;
    } //4

    public float getSL( int n,float a) {
        return  n + a ;
    } //5


    // overloading method

     // noted : Tên method giống nhau (diều kiện bắt buộc)
    // kết hợp với 1 trong 3 ý sau là method overloading .

     // 1 .Số lượng paramter khác nhau
     // 2. Cùng số lượng params ,nhưng khác kiểu dữ liệu
     // 3. Cùng số lượng params, nhưng kiểu dữ liệu hoán đổi cho nhau



   // byte → short → int → long→ float → double






}
