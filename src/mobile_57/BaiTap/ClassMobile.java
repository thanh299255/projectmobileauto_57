package mobile_57.BaiTap;

public class ClassMobile {

    String nameClass ;
    int slHs ;
    String nameHs ;
     float diemToan ;
    double  diemVan;
    String Khoa ;


    // Ham tra ve kieu : int

    public int getSlHs(String nameClass) {
        if(nameClass.equals("9A")) {
            slHs = 45 ;
        }
        else {
            System.out.print("Not found Class Name is : "+ nameClass);
        }
        return  slHs;

    }



    // Ham tra ve kieu : float
    public  float getDiemToanFollowNameHs(String nameHs) {
        if(nameHs.equals("Thu")) {
           diemToan = 9 ;
        }
        else {
            System.out.print("Not found  Name HS is : "+ nameHs);
        }
        return  diemToan;

    }

    // Ham tra ve kieu : flodiemVanat
    public double getDiemVanFollowNameHs(String nameHs) {
        if(nameHs.equals("Thu")) {
            diemVan = 8 ;
        }
        else {
            System.out.print("Not found  Name HS is : "+ nameHs);
        }
        return  diemVan;

    }

    // Ham tra ve kieu : String
    public String getKhoaFollowClassName(String nameClass) {
        if(nameClass.equals("CNTT_01")) {
            Khoa = "Khoa  CNTT" ;
        }
        else {
            System.out.print("Not found className is : "+ nameClass);
        }
        return  Khoa;

    }


  // contructor 1
    ClassMobile (){
    }

    // contructor 2
    ClassMobile (String nameClass){
        this.nameClass = nameClass;
    }

    // contructor 3
    ClassMobile (String nameClass, String nameHs){
        this.nameClass = nameClass;
        this.nameHs =nameHs;
    }

    // contructor 4
    ClassMobile (String nameHs,float diemToan){
        this.diemToan = diemToan;
        this.nameHs =nameHs;
    }




}
