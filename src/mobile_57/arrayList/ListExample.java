package mobile_57.arrayList;

import java.util.*;

public class ListExample {

  //List
    public static List<Integer> getSl()  {
        // index : vi tri mang ( luon bat dau la 0 ,1,2,3,4...)
        int [] a = new int[10];//khoi tao do dai mang

            List<Integer> listInt = new ArrayList<>();

//            ArrayList<Integer> abb = new ArrayList<>();

            // add interger ( tu random value : index mang (bat dau la 0)
            listInt.add(1);
            listInt.add(3);
            listInt.add(2);

            // add : index , value(integer)
            listInt.add(0, 50);
            listInt.add(1, 100);

            // remove index mang
//            listInt.remove(3);
            System.out.print(a.length+"\n");

            return listInt;

    }

    //List<Integer>  ==> List<String> (Float,Double)


    // Map


    // Map ?
    public static Map<String,String> getMap()  {
        // index : vi tri mang ( luon bat dau la 0 ,1,2,3,4...)

        Map<String,String> listInt = new HashMap<>();  //<String,String> ,<String,object>
        listInt.put("request_id","593c4185-bda2-4946-802b-9e5a407e530c#5463");
        listInt.put("code","200000");
        listInt.put("message","Success");

        // noted :
        //1 . Key ,value : key giong nhau ==> no se lay gia tri add cuoi cung

        return listInt;

    }

    // HashMap ?
    public static HashMap<String,Object> getHashMap()  {
        // index : vi tri mang ( luon bat dau la 0 ,1,2,3,4...)

        HashMap<String,Object> listInt = new HashMap<>();  //<String,String> ,<String,object>
        listInt.put("request_id","593c4185-bda2-4946-802b-9e5a407e530c#5463");
        listInt.put("code","200000");
        listInt.put("message","Success");

        listInt.get("code"); //lay gia tri(value) theo key


        HashMap<String,Object> listInt1 = new HashMap<>();
        listInt1.putAll(listInt);  //sao chep lai hashmap co gia tri


//        listInt1.clear(); // xoa sach HashMap

//       System.out.println(listInt1.size()+"\n") ;

//        System.out.println(listInt1.clone());

//        System.out.println(listInt1.remove("message"));

//        listInt1.remove("","");

//        listInt1.putIfAbsent("Test","1111");

//        System.out.println(listInt1.entrySet());



        for (Map.Entry<String, Object> entry : listInt1.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

//            listInt.get("code");

            System.out.print(key +" "+ value+"\n");

        }


        // noted :
        //1 . Key ,value : key giong nhau ==> no se lay gia tri add cuoi cung

        return listInt1;

    }


    // HashSet ?
    public static HashSet<String> getHashSet()  {
        // index : vi tri mang ( luon bat dau la 0 ,1,2,3,4...)

        HashSet<String> listInt = new HashSet<>();  //
        listInt.add("10");
        listInt.add("20");
        listInt.add("10");
        listInt.add("10");
        listInt.add("10");
        listInt.add("10");
        listInt.add("20");
        listInt.add("20");
        listInt.add("20");

        // noted :
        //1 . No loc trung du lieu cac gia tri cua mang : 1 phan tu cho cac phan tu trung nhau

        return listInt;

    }


  // LinkedList ( List)
  public static LinkedList<String> getLinkedList()  {

      LinkedList<String> listInt = new LinkedList<>();  //
      listInt.add("10");
      listInt.add("20");
      listInt.add("10");
      listInt.add("10");
      listInt.add("10");
      listInt.add("10");
      listInt.add("20");
      listInt.add("20");
      listInt.add("20");

      return listInt;

  }






    public static void main (String [] args){

//      System.out.print(getSl());
//        System.out.print(getMap());
//        System.out.print(getHashMap());
        System.out.print(getLinkedList());

//        System.out.print(getHashSet());

    }
}
