package mobile_57.superkeyword;

class Parentclass
{
    //no-arg constructor
    Parentclass(){
        System.out.println("no-arg constructor of parent class");
    }  //1



    //arg or parameterized constructor
    Parentclass(String str){
        System.out.println("parameterized constructor of parent class");
    } //2



    //arg or parameterized constructor
    Parentclass(String str ,int a){
        System.out.println("parameterized constructor of parent class");
    } //2
}


class Subclass extends Parentclass
{
//    private static final String phone = "1111";
//    private static final int sl = 111;
//

    //    String phone = "098877777" ;
//    int sl =10;
    Subclass(){
        /* super() must be added to the first statement of constructor
         * otherwise you will get a compilation error. Another important
         * point to note is that when we explicitly use super in constructor
         * the compiler doesn't invoke the parent constructor automatically.
         */

        super("" ,1);  //supper keyword

//        Parentclass  a =  new Parentclass("aaa",10);

        System.out.println("Constructor of child class");

    }


    Subclass(String a){
        /* super() must be added to the first statement of constructor
         * otherwise you will get a compilation error. Another important
         * point to note is that when we explicitly use super in constructor
         * the compiler doesn't invoke the parent constructor automatically.
         */

        super("");  //supper keyword

//        Parentclass  a =  new Parentclass("aaa",10);

        System.out.println("Constructor of child class");

    }



    void display(){
        System.out.println("Hello");
    }


    public static void main(String args[]){
        Subclass obj= new Subclass();
        obj.display();
    }
}
