package mobile_57.superkeyword;

class Parentclass1
{
    //Overridden method
    void display1(){
        System.out.println("Parent class method");
    }  //1 method Overridden

    public String getPhone(){
      String  phone = "092939293293";
        return  phone ;
    }


}
class Subclass1 extends Parentclass1
{
    //Overriding method
    void display(){
        System.out.println("Child class method");
    }


 // Overriding method
    void printMsg(){
        //This would call Overriding method
        display();
        //This would call Overridden method
        super.getPhone();   // ke thua tu extends class cha



        // 1. super.(method) ==> call method class parent
        // 2 . super() ,super(params) , super.(name Method)
        // ==> ton tai khi co : extends
        // 3 . super :  co 2 dang
        // + super() , super(params) : dung trong constructor
        // + super.(Name method) : dùng cho xử lý method child
         //4 . super.(name method ) ==> businuss dính liền info nhau
        // 5. có   extends ==> nen sai super keyword



    }


//super.getPhone();  dung cho call method Overridden

//    super();
//    super("paramter") ;  ==> constructor

    public static void main(String args[]){
        Subclass1 obj= new Subclass1();
        obj.printMsg();


    }



}
